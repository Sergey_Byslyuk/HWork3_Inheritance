package Work;

/**
 * Created by user on 04.09.2017.
 */
public class Application {
    public static void main(String[] args) {
        Application app = new Application();
        app.runTheApp();
    }
    private void runTheApp() {
        Property fridge   = new Fridge(123.0f, "white", "LG", 500.0f, -18, 4,'A');
        Property property = new Fridge(123.0f, "black", "Samsung", 500.0f, -18,4, 'A');
        Property tv       = new TvSet(1200f, "black", "LG", 32.0f, "LCD");
        Property laptop   = new Laptop(2000.0f, "grey", "HP", "Intel", "Asus",
                                        4096, 256, "smt" );
        Property car      = new Car(8050.0f, "red", "KIA", 250, "supercar", "minivan");
        Property[] properties = new Property[5];
        properties[0] = fridge;
        properties[1] = property;
        properties[2] = tv;
        properties[3] = laptop;
        properties[4] = car;
        for (Property p : properties) {
            System.out.println(p.getInfo());
        }
    }
}
