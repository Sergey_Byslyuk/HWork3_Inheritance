package Work;

/**
 * Created by user on 05.09.2017.
 */
public class Car extends Property {
    private int speed;
    private String vehicle;
    private String body;

    private Car(float price, String color, String vendor){
        super( price," Car",  color, vendor);
    }

    public Car(float price, String color, String vendor, int speed, String vehicle, String body) {
        this(price, color, vendor);
        this.speed = speed;
        this.vehicle = vehicle;
        this.body = body;
    }

    public int getSpeed() {

        return speed;
    }

    public void setSpeed(int speed) {
        this.speed = speed;
    }

    public String getVehicle() {
        return vehicle;
    }

    public void setVehicle(String vehicle) {
        this.vehicle = vehicle;
    }

    public String getBody() {
        return body;
    }

    public void setBody(String body) {
        this.body = body;
    }

    @Override
    public String toString() {
        return super.toString()  +
                " speed=" + speed +
                ", vehicle='" + vehicle + '\'' +
                ", body='" + body + '\'' +
                '}';
    }
}
