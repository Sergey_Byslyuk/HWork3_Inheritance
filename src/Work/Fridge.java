package Work;

/**
 * Created by user on 04.09.2017.
 */
public class Fridge extends Property {
    private float volume;
    private int minTemp;
    private int maxTemp;
    private char energySafeClass;

    private Fridge(float price, String color, String vendor) {
        super(price, "Fridge", color, vendor);
    }

    public Fridge(float price, String color, String vendor, float volume, int minTemp, int maxTemp, char energySafeClass) {
        this(price, color, vendor);
        this.volume = volume;
        this.minTemp = minTemp;
        this.maxTemp = maxTemp;
        this.energySafeClass = energySafeClass;
    }

    public float getVolume() {
        return volume;
    }

    public void setVolume(float volume) {
        this.volume = volume;
    }

    public int getMinTemp() {
        return minTemp;
    }

    public void setMinTemp(int minTemp) {
        this.minTemp = minTemp;
    }

    public int getMaxTemp() {
        return maxTemp;
    }

    public void setMaxTemp(int maxTemp) {
        this.maxTemp = maxTemp;
    }

    public char getEnergySafeClass() {
        return energySafeClass;
    }

    public void setEnergySafeClass(char energySafeClass) {
        this.energySafeClass = energySafeClass;
    }

    @Override
    public String toString() {
        final StringBuffer sb = new StringBuffer(super.toString());
        sb.append(" volume=").append(volume);
        sb.append(", minTemp=").append(minTemp);
        sb.append(", maxTemp=").append(maxTemp);
        sb.append(", energySafeClass=").append(energySafeClass);
        sb.append('}');
        return sb.toString();
    }
}

