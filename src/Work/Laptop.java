package Work;

/**
 * Created by user on 05.09.2017.
 */
public class Laptop extends Property{
    private String СPU;
    private String motherboard;
    private int ram;
    private float diaganal;
    private String screenType;

    private Laptop(float price,  String color, String vendor){
        super(price, "Laptop", color, vendor);
    }



    public String getСPU() {
        return СPU;
    }

    public void setСPU(String СPU) {
        this.СPU = СPU;
    }

    public String getMotherboard() {
        return motherboard;
    }

    public void setMotherboard(String motherboard) {
        this.motherboard = motherboard;
    }

    public int getRam() {
        return ram;
    }

    public void setRam(int ram) {
        this.ram = ram;
    }

    public float getDiaganal() {
        return diaganal;
    }

    public void setDiaganal(float diaganal) {
        this.diaganal = diaganal;
    }

    public String getScreenType() {
        return screenType;
    }

    public void setScreenType(String screenType) {
        this.screenType = screenType;
    }

    public Laptop(float price,  String color, String vendor, String СPU, String motherboard, int ram,
                  float diaganal, String screenType) {
        this(price, color,vendor);
        this.СPU = СPU;
        this.motherboard = motherboard;
        this.ram = ram;
        this.diaganal = diaganal;
        this.screenType = screenType;
    }

    @Override
    public String toString() {
        return super.toString() +
                " СPU=" + СPU +
                ", motherboard='" + motherboard + '\'' +
                ", ram=" + ram +
                ", diaganal=" + diaganal +
                ", screenType='" + screenType + '\'' +
                '}';
    }
}