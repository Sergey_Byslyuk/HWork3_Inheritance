package Work;

/**
 * Created by user on 04.09.2017.
 */
public class Property {
    protected float price;
    private String name;
    protected String color;
    protected String vendor;

    public Property(float price, String name, String color, String vendor) {
        this.price = price;
        this.name = name;
        this.color = color;
        this.vendor = vendor;
    }

    public float getPrice() {
        return price;
    }

    public void setPrice(float price) {
        this.price = price;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public String getVendor() {
        return vendor;
    }

    public void setVendor(String vendor) {
        this.vendor = vendor;
    }

    public String getName() {
        return name;
    }

    @Override
    public String toString() {
        final StringBuffer sb = new StringBuffer();
        sb.append(" price=").append(price);
        sb.append(", name='").append(name).append('\'');
        sb.append(", color='").append(color).append('\'');
        sb.append(", vendor='").append(vendor).append('\'');
        return sb.toString();
    }

    public String getInfo(){
        return toString();
    }
}

