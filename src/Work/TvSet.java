package Work;

/**
 * Created by user on 04.09.2017.
 */
public class TvSet extends Property {
    private float diagonal;
    private String screenType;

    private TvSet(float price, String color, String vendor) {
        super(price, "TV Set", color, vendor);
    }

    public TvSet(float price, String color, String vendor, float diagonal, String screenType) {
        this(price, color, vendor);
        this.diagonal = diagonal;
        this.screenType = screenType;
    }

    public float getDiagonal() {
        return diagonal;
    }

    public void setDiagonal(float diagonal) {
        this.diagonal = diagonal;
    }

    public String getScreenType() {
        return screenType;
    }

    public void setScreenType(String screenType) {
        this.screenType = screenType;
    }

    @Override
    public String toString() {
        final StringBuffer sb = new StringBuffer(super.toString());
        sb.append(" diagonal=").append(diagonal);
        sb.append(", screenType='").append(screenType).append('\'');
        sb.append('}');
        return sb.toString();
    }
}

